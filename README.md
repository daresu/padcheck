# padCheck #

  ノートPCのタッチパッドの有効・無効を制御するためのプログラムです。  
  QT5.6を使用して作成しています。

# 必須ライブラリ #
* libnotify-dev
* xinput