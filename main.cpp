#include "widget.h"
#include <QApplication>
#include <QTextCodec>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // ライブラリのPathに実行フォルダを追加
    a.addLibraryPath(a.applicationDirPath());

    Widget w;
//    w.show();

    return a.exec();
}
