#-------------------------------------------------
#
# Project created by QtCreator 2014-12-16T11:37:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = padChecker
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui

RESOURCES += \
    rc.qrc

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += libnotify
