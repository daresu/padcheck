#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QCloseEvent>
#include <QMenu>
#include <QSystemTrayIcon>
#include <QProcess>
#include <QTimer>
#undef signals
extern "C"
{
    #include <libnotify/notify.h>
}

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void showEvent(QShowEvent *);
    void closeEvent(QCloseEvent *);

private slots:
    void on_pBtn_Save_clicked();
    void processOutput();
    void processFinished();
    void processError(QProcess::ProcessError error);
    void timerUpdate();

private:
    Ui::Widget *ui;

    void createTrayIcon();
    void createActions();
    void setPos();
    void getDeviceList();
    bool isDeviceMouse(QString);
    bool isDevicePad(QString);
    bool isDeviceEnable(QString);
    void setDeviceEnable(QString, bool);

    void notify_show(QString title, QString message);

    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
    QAction *showWidget;
    QAction *quitAction;
    QProcess *xinputMainProcess;
    QProcess *xinputSubProcess;

    QList<QString> idList;

    QTimer *timer;

    bool m_MouseFlg;
};

#endif // WIDGET_H
