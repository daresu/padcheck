#include "widget.h"
#include "ui_widget.h"

#include <QDesktopWidget>
#include <QMessageBox>

/*!
 * \brief コンストラクタ
 * \param parent
 */
Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    //! 設定画面の表示場所を設定する
    setPos();

    //! メニューの内部処理
    createActions();
    //! メニューの作成
    createTrayIcon();

    //! プロセス実行変数
    xinputMainProcess = new QProcess(this);
    //! プロセスが終了した時に finished シグナル発信
    connect(xinputMainProcess, SIGNAL(finished(int)), this, SLOT(processFinished()));
    //! プロセスから出力があって読み込み可能になったら readyRead シグナル発信
    connect(xinputMainProcess, SIGNAL(readyRead()), this, SLOT(processOutput()));
    //! プロセスがエラーを出力したら error シグナル発信
    connect(xinputMainProcess, SIGNAL(error(QProcess::ProcessError)), this, SLOT(processError(QProcess::ProcessError)));

    //! プロセス実行変数
    xinputSubProcess = new QProcess(this);

    //! マウス存在フラグを初期設定
    m_MouseFlg = false;
    //! デバイスの一覧を取得して、状態を設定する
    xinputMainProcess->start("xinput list");

    //! 定期実行？
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
    timer->start(3000);
}

/*!
 * \brief Widget::~Widget
 * デストラクタ
 */
Widget::~Widget()
{
    delete ui;

    //! プロセスが動いていたら殺す
    if (xinputMainProcess->state() == QProcess::Running) {
        xinputMainProcess->kill();
        xinputMainProcess->waitForFinished();
    }
    delete xinputMainProcess;

    //! プロセスが動いていたら殺す
    if (xinputSubProcess->state() == QProcess::Running) {
        xinputSubProcess->kill();
        xinputSubProcess->waitForFinished();
    }
    delete xinputSubProcess;
}

/*!
 * \brief Widget::setPos
 * 設定画面の表示位置を設定
 */
void Widget::setPos()
{
    //! 画面の描画可能位置を取得する（タスクバーを考慮したサイズ）
    QDesktopWidget *desktop = qApp->desktop();
    QRect dRect = desktop->availableGeometry();
    //! 画面の右上に表示する
    move(dRect.width() - geometry().width(), dRect.y());
}

/*!
 * \brief Widget::createTrayIcon
 * トレイアイコンを作成する
 */
void Widget::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(showWidget);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    //! アイコン画像を渡して初期化しないとXFCE4でトレイアイコンが表示されない
    trayIcon = new QSystemTrayIcon(QIcon(":img/pad_off.png"), this);
   // trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->show();
//    trayIcon->showMessage(tr("gat2"),tr("Welcome to gat2!"),QSystemTrayIcon::Information,1500);
}

/*!
 * \brief Widget::createActions
 * イベントを作成する
 */
void Widget::createActions()
{
    showWidget = new QAction("設定", this);
    connect(showWidget, SIGNAL(triggered()), this, SLOT(showNormal()));

    quitAction = new QAction("終了", this);
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
}

void Widget::showEvent(QShowEvent *event)
{
    setPos();
    event->accept();
}

/*!
 * \brief Widget::closeEvent
 * 終了イベント
 * \param event 実行されたイベント
 */
void Widget::closeEvent(QCloseEvent *event)
{
    if (trayIcon->isVisible()) {
        hide();
        event->ignore();
    }
}

/*!
 * \brief Widget::processOutput
 * 取得したデバイスの内容をチェックする
 */
void Widget::processOutput()
{
    getDeviceList();
}

/*!
 * \brief Widget::processFinished
 * デバイスの一覧取得完了
 */
void Widget::processFinished()
{
//    trayIcon->showMessage(tr("メッセージ"), tr("プロセスが終了しました"), QSystemTrayIcon::Information, 1500);
}

/*!
 * \brief Widget::processError
 * エラー発生
 * \param error
 */
void Widget::processError(QProcess::ProcessError error)
{
    switch (error) {
        case QProcess::FailedToStart:
            notify_show("エラー", "xinputが見つかりませんでした。");
            break;

        case QProcess::Crashed:
            notify_show("エラー", "xinputが強制終了しました。");
            break;

        case QProcess::Timedout:
            notify_show("エラー", "xinputがタイムアウトが発生しました。");
            break;

        default:
            notify_show("エラー", "xinputで想定外のエラーが発生しました。");
            break;

    }

}

/*!
 * \brief Widget::on_pBtn_Save_clicked
 * 適用ボタンが押された時
 */
void Widget::on_pBtn_Save_clicked()
{
//    xinputListProcess->start("xinput list");
    if (idList.length() > 0) {
        setDeviceEnable(idList[0], ui->chkBox_Pad->isChecked());
    }
}

/*!
 * \brief Widget::getDeviceList
 * タッチパッドのデバイスを取得する
 */
void Widget::getDeviceList()
{
    QByteArray data;
    QString str;
    int idx_s;
    int idx_e;
    QString id;
    bool mouseFlg;

    mouseFlg = false;

    while((data = xinputMainProcess->readLine()) != "") {
        str = QString(data.data());
        idx_s = str.indexOf("id=");
        idx_e = str.indexOf("\t", idx_s);
        idx_s += 3;
        id = str.mid(idx_s, idx_e - idx_s);
        if (isDeviceMouse(id)) {
            mouseFlg = true;
        }
        if (isDevicePad(id) == true) {
            idList.append(id);

            ui->chkBox_Pad->setChecked(isDeviceEnable(id));
        }
    }
    if (idList.length() == 0) {
        ui->chkBox_Pad->setEnabled(false);
        trayIcon->setIcon(QIcon(":img/pad_off.png"));
        notify_show("タッチパッドの状態",
                    "タッチパッドはありません");
        //trayIcon->showMessage("タッチパッドの状態",
        //                      "タッチパッドはありません",
        //                      QSystemTrayIcon::Information, 3000);
    } else {
        if (m_MouseFlg != mouseFlg) {
            m_MouseFlg = mouseFlg;
            if (m_MouseFlg == true) {
                ui->chkBox_Pad->setChecked(false);
                setDeviceEnable(idList[0], false);
                trayIcon->setIcon(QIcon(":img/pad_off.png"));
                notify_show("タッチパッドの状態",
                            "マウスが見つかったため、タッチパッドを無効にしました");
                //trayIcon->showMessage("タッチパッドの状態",
                //                      "マウスが見つかったため、タッチパッドを無効にしました",
                //                      QSystemTrayIcon::Information, 3000);
            } else {
                ui->chkBox_Pad->setChecked(true);
                setDeviceEnable(idList[0], true);
                trayIcon->setIcon(QIcon(":img/pad_on.png"));
                notify_show("タッチパッドの状態",
                            "マウスが見つからなかったため、タッチパッドを有効にしました");
                //trayIcon->showMessage("タッチパッドの状態",
                //                      "マウスが見つからなかったため、タッチパッドを有効にしました",
                //                      QSystemTrayIcon::Information, 3000);
            }
        }
    }
}

/*!
 * \brief Widget::isDeviceMouse
 * デバイスがマウスかどうかの判定
 * \param id デバイスのID
 * \return 判定結果
 */
bool Widget::isDeviceMouse(QString id)
{
    QByteArray data;
    QString str;

    xinputSubProcess->start("xinput list-props " + id);
    xinputSubProcess->waitForFinished();

    while((data = xinputSubProcess->readLine()) != "") {
        str = QString(data.data());
        if (str.indexOf("Mouse") != -1) {
            //! マウスだった
            return true;
        }
    }

    return false;
}

/*!
 * \brief Widget::isDevicePad
 * デバイスがタッチパッドかどうかの判定
 * \param id デバイスのID
 * \return 判定結果
 */
bool Widget::isDevicePad(QString id)
{
    QByteArray data;
    QString str;

    xinputSubProcess->start("xinput list-props " + id);
    xinputSubProcess->waitForFinished();

    while((data = xinputSubProcess->readLine()) != "") {
        str = QString(data.data());
        if (str.indexOf("Synaptics Off") != -1) {
            //! タッチパッドだった
            return true;
        }
    }

    return false;
}

/*!
 * \brief Widget::isDeviceEnable
 * デバイスの状態を取得する
 * \param id デバイスのID
 * \return 判定結果
 */
bool Widget::isDeviceEnable(QString id)
{
    QByteArray data;
    QString str;
    int idx;

    xinputSubProcess->start("xinput list-props " + id);
    xinputSubProcess->waitForFinished();

    while((data = xinputSubProcess->readLine()) != "") {
        str = QString(data.data());
        if (str.indexOf("Device Enabled") != -1) {
            idx = str.indexOf(":") + 2;

            if (str.mid(idx, 1) == "1") {
                trayIcon->setIcon(QIcon(":img/pad_on.png"));
                //trayIcon->showMessage("タッチパッドの状態", "タッチパッドは有効です。", QSystemTrayIcon::Information, 1500);
                return true;
            }else{
                break;
            }
        }
    }

    trayIcon->setIcon(QIcon(":img/pad_off.png"));
    //trayIcon->showMessage("タッチパッドの状態", "タッチパッドは無効です。", QSystemTrayIcon::Information, 1500);
    return false;
}

/*!
 * \brief Widget::setDeviceEnable
 * デバイスの状態を設定する
 * \param id デバイスのID
 * \param value true・false
 */
void Widget::setDeviceEnable(QString id, bool value)
{
    QString cmd;

    cmd = "xinput set-int-prop " + id + " \"Device Enabled\" 8 ";

    if (value == true) {
        cmd += "1";
        trayIcon->setIcon(QIcon(":img/pad_on.png"));
        notify_show("タッチパッドの状態", "有効にしました");
        //trayIcon->showMessage("タッチパッドの状態", "有効にしました", QSystemTrayIcon::Information, 1500);
    }else{
        cmd += "0";
        trayIcon->setIcon(QIcon(":img/pad_off.png"));
        notify_show("タッチパッドの状態", "無効にしました");
        //trayIcon->showMessage("タッチパッドの状態", "無効にしました", QSystemTrayIcon::Information, 1500);
    }

    xinputSubProcess->start(cmd);
    xinputSubProcess->waitForFinished();
}

/*!
 * \brief Widget::timerUpdate
 * 定期実行
 */
void Widget::timerUpdate()
{
    //trayIcon->showMessage("QTIMER", "テスト", QSystemTrayIcon::Information, 500);
    //! 自動更新するかをチェック
    if (ui->chkBox_Auto->isChecked() == true) {
        //! デバイスの一覧を取得する
        xinputMainProcess->start("xinput list");
    }
}

/*!
 * \brief notifyにメッセージを送信して表示する
 * \param title タイトル
 * \param message メッセージ
 */
void Widget::notify_show(QString title, QString message)
{
    notify_init(title.toUtf8().constData());
    NotifyNotification * MSG = notify_notification_new (title.toUtf8().constData(), message.toUtf8().constData(), "dialog-information");
    notify_notification_show (MSG, NULL);
    g_object_unref(G_OBJECT(MSG));
    notify_uninit();
}
